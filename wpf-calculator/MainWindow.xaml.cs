﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf_calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            result = null;
            memory = null;
            rString = new ResultString() { Value = "" };
            currentResult.DataContext = rString;
        }

        Nullable<Double> memory;
        Nullable<Double> result;
        char op;
        ResultString rString;

        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 7;
            rString.Value += 7;
        }

        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 8;
            rString.Value += 8;
        }

        private void Button9_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 9;
            rString.Value += 9;
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            Result.Text = "";
            rString.Value = "";
            result = null;
            memory = null;
            op = new char();
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 2;
            rString.Value += 2;
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 5;
            rString.Value += 5;
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 4;
            rString.Value += 4;
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 3;
            rString.Value += 3;
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 1;
            rString.Value += 1;
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            Result.Text += 6;
            rString.Value += 6;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (op != '+' || op != '-' || op != '*' || op != '/')
            {
                AddNumber();
                op = '+';
                Result.Text = "";
                if (rString.Value.Last() != '+') rString.Value += '+';
            }
        }

        private void ButtonSubstract_Click(object sender, RoutedEventArgs e)
        {
            if (op != '+' || op != '-' || op != '*' || op != '/')
            {
                AddNumber();
                op = '-';
                Result.Text = "";
                if (rString.Value.Last() != '-') rString.Value += '-';
            }
        }

        private void ButtonMultiply_Click(object sender, RoutedEventArgs e)
        {
            if (op != '+' || op != '-' || op != '*' || op != '/')
            {
                AddNumber();
                op = '*';
                Result.Text = "";
                if (rString.Value.Last() != '*') rString.Value += '*';
            }
        }

        private void ButtonDivide_Click(object sender, RoutedEventArgs e)
        {
            if (op != '+' || op != '-' || op != '*' || op != '/')
            {
                AddNumber();
                op = '/';
                Result.Text = "";
                if (rString.Value.Last() != '/') rString.Value += '/';
            }
        }

        private void ButtonResult_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (result == null) { result = Double.Parse(Result.Text); }
                memory = Calculate(memory.Value, result.Value, op);
                result = null;
                Result.Text = memory.ToString();
                op = '=';
            }
            catch (ArgumentNullException ex)
            {
                result = null;
                memory = null;
                op = new char();
            }
            catch (FormatException ex)
            {
                op = new char();
            }
            finally
            {
                rString.Value += "=" + Result.Text;
            }
        }

        private Double Calculate (Double n1, Double n2, char o)
        {
            Double r = new double();

            switch (op)
            {
                case '+':
                    r = n1 + n2;
                    break;

                case '-':
                    r = n1 - n2;
                    break;

                case '*':
                    r = n1 * n2;
                    break;

                case '/':
                    r = n1 / n2;
                    break;
            }

            op = new char();

            return r;
        }

        private void ButtonPoint_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(Result.Text, "[.]"))
            {
                Result.Text += ".";
                rString.Value += ".";
            }

        }

        private void Button0_Click(object sender, RoutedEventArgs e)
        {
            if (Result.Text.Length > 0)
            {
                Result.Text += 0;
                rString.Value += 0;
            }
        }

        private void AddNumber()
        {
            try
            {
                if (op != '=')
                {
                    if (memory == null) memory = Double.Parse(Result.Text);
                    else
                    {
                        if (result == null) result = Double.Parse(Result.Text);
                        memory = Calculate(memory.Value, result.Value, op);
                        Result.Text = "";
                        result = null;
                    }
                }
            }
            catch (ArgumentNullException ex)
            {
                result = null;
                memory = null;
                op = new char();
            }
            catch (FormatException ex) { op = new char(); }
        }
    }

    public class ResultString : INotifyPropertyChanged
    {
        private String _value;

        public String Value
        {
            get { return _value; }
            set { _value = value; NotifyPropertyChanged("Value"); }
        }

        public ResultString() { }
        public ResultString(String val) { _value = val; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
